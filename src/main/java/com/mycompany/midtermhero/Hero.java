/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midtermhero;

/**
 *
 * @author BOAT
 */
public class Hero {
    protected String name;
    protected int Age = 0;
    protected String skinColor;
    
    public Hero(String name,String skinColor,int Age){
        System.out.println("Hero created");
        this.name = name;
        this.skinColor = skinColor;
        this.Age = Age;
    }
    public void superPower(){
        System.out.println("SuperPower Hero");
    }
    public void title(){
        System.out.println("Title Hero");
        System.out.println("name: "+this.name+" skinColor: "
                + this.skinColor+" Age: "+this.Age);
    }
     public void superPower1(){
        System.out.println("SuperPower Hero");
    }
    public void title1(){
        System.out.println("Title Hero");
        System.out.println("name: "+this.name+" skinColor: "
                + this.skinColor+" Age: "+this.Age);
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return Age;
    }

    public String getSkinColor() {
        return skinColor;
    }
    
}
